package com.example.dominik.mtaaaplikacia;

import android.util.Log;

import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;

/**
 * Created by Dominik on 5/11/2016.
 */
public class SocketManager {

    Socket socket = null;

    public SocketManager() {

        IO.Options opts = new IO.Options();

        opts.secure = false;
        opts.port = 1341;
        opts.reconnection = true;
        opts.forceNew = true;
        opts.timeout = 5000;

        try{
            socket = IO.socket("http://sandbox.touch4it.com:1341/?__sails_io_sdk_version=0.12.1", opts);
            socket.connect();
        }catch(URISyntaxException e){
            e.printStackTrace();
        }
    }

    public void makeRequest(String method, JSONObject obj) {
        socket.emit(method, obj, new Ack() {
            @Override
            public void call(Object... args) {
                String text = null;
                ArrayList<String> reads = new ArrayList<String>();
                try {
                    text = Arrays.toString(args);//server odpoved
                    System.out.println(text);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (!text.equals("")) {
                    reads.add(text);
                    Log.d("Debug", "put response: " + reads.toString());
                }
            }
        });
    }

}

