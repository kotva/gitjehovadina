package com.example.dominik.mtaaaplikacia;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class ZoomActivity extends AppCompatActivity {
    private ImageView mImageView;
    private ImageView imgListener;
  //  private String imgUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom);

        //imgUrl = getIntent().getStringExtra("pic");
        mImageView = (ImageView)findViewById(R.id.zoomIV);
        SimpleImageLoader imageLoaderTask = new SimpleImageLoader(mImageView);
        imageLoaderTask.execute(getIntent().getStringExtra("pic"));
        imgListener = (ImageView)findViewById(R.id.zoomIV);
        imgListener.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ZoomActivity.this.finish();
            }
        });
    }
}
