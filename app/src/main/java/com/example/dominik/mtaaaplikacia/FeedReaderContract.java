package com.example.dominik.mtaaaplikacia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by Dominik on 20. 4. 2016.
 */
public final class FeedReaderContract {
    public FeedReaderContract() {}

    public static abstract class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "restaurants";
        public static final String COLUMN_NAME_RST_UUID = "rst_uuid";
        public static final String COLUMN_NAME_RST_NAME = "rst_name";
        public static final String COLUMN_NAME_RST_ADDRESS = "rst_address";
        public static final String COLUMN_NAME_RST_IMG = "rst_img";
        public static final String COLUMN_NAME_RST_OWNER = "rst_owner";
        public static final String COLUMN_NAME_RST_CAT = "rst_cat";
    }

    private static final String COMA = ",";
    private static final String TEXT = " TEXT";
    private static final String SQL_CREATE_ENTRIES = "CREATE TABLE " + FeedEntry.TABLE_NAME + " (" +
            FeedEntry._ID + " INTEGER PRIMARY KEY," + FeedEntry.COLUMN_NAME_RST_UUID + TEXT + COMA + FeedEntry.COLUMN_NAME_RST_NAME + TEXT + COMA +
            FeedEntry.COLUMN_NAME_RST_ADDRESS + TEXT + COMA + FeedEntry.COLUMN_NAME_RST_CAT + TEXT + COMA + FeedEntry.COLUMN_NAME_RST_IMG + TEXT + COMA +
            FeedEntry.COLUMN_NAME_RST_OWNER + TEXT + " )";

    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + FeedEntry.TABLE_NAME;

    public class FeedReaderDBHelper extends SQLiteOpenHelper {

        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "FeedReader.db";

        public FeedReaderDBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }


        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_ENTRIES);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(SQL_DELETE_ENTRIES);
            onCreate(db);
        }
    }
}